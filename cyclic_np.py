import numpy as np
import matplotlib.pyplot as plt

WIDTH = 64
HEIGHT = 48
NUM_STATES = 16

def create_random_array(width, height):
    return np.random.randint(0, NUM_STATES, size=(width, height))

board = create_random_array(WIDTH, HEIGHT)
print(board)

def cell_rule(board, x, y):
    """Evaluate the rule for a single cell, returning the cell's new state
    (which can be the same as the old state): if the cell is in state N
    and a neighbour is in state N+1, the cell will enter state N+1 as well.
    """
    value = board[x,y]
    successor = (value + 1) % NUM_STATES
    indices = []
    width, height = board.shape
    for i in range(x-1, x+2):
        for j in range(y-1, y+2):
            if ((i,j) != (x,y) and
                i > 0 and j > 0 and
                i < width and j < height):
                indices.append((i,j))

    neighborhood = np.array(indices)
    neighbors = board[neighborhood]
    if np.any(neighbors == successor):
        return successor
    else:
        return value


def step(board):
    "Perform one iteration step of the cellular automaton"
    it = np.nditer([board, None], flags=['multi_index'])
    for old, new in it:
        new[...] = cell_rule(board, *it.multi_index)
    return it.operands[1]

board = step(board)
fig = plt.imshow(board)

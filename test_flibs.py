#!/usr/bin/env python

import unittest
import flibs

class TestFlibs(unittest.TestCase):
    def test_random(self):
        flib = flibs.Flib.random(5)
        self.assertEqual(len(flib.transitions), 10)

    def test_roundtrip(self):
        flib = flibs.Flib.random(5)
        genome = flib.as_genome()
        self.assertEqual(len(genome), 5 * 2 * 2)

        copy = flibs.Flib.from_genome(genome)
        self.assertEqual(flib.transitions, copy.transitions)

    def test_transition(self):
        # For this transition table,
        flib = flibs.Flib()
        flib.transitions[('A', 0)] = ('B', 1)
        flib.transitions[('A', 1)] = ('B', 0)
        flib.transitions[('B', 0)] = ('A', 1)
        flib.transitions[('B', 1)] = ('A', 0)

        # Then from state A, a '1' bit results in a '0' output
        self.assertEqual(flib.predict(1), 0)
        # And a transition to state B
        self.assertEqual(flib.state, 'B')

        # And in state 'B', a '0' bit results in a '1' output
        self.assertEqual(flib.predict(0), 1)
        # And a transition to state 'B'
        self.assertEqual(flib.state, 'A')

    def test_scoring(self):
        flib = flibs.Flib()
        flib.transitions[('A', 0)] = ('B', 1)
        flib.transitions[('A', 1)] = ('B', 0)
        flib.transitions[('B', 0)] = ('A', 1)
        flib.transitions[('B', 1)] = ('A', 0)

        # Transition table will always predict '1', so it should score 0.
        environment = '000'
        self.assertEqual(flib.score(environment), 0)

        # If the transition table always predicts '1', it will score 100.
        flib.transitions[('A', 1)] = ('B', 1)
        flib.transitions[('B', 1)] = ('A', 1)
        environment = '111'
        self.assertEqual(flib.score(environment), 100)


if __name__ == '__main__':
    unittest.main()

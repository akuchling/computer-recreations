
import unittest
import cyclic

class TestRule(unittest.TestCase):
    def setUp(self):
        self.board = cyclic.Board(width=3, height=3)
        self.board.board = {
            (0,0): 1, (1,0): 1, (2,0): 1,
            (0,1): 1, (1,1): 0, (2,1): 1,
            (0,2): 1, (1,2): 1, (2,2): 1
            }

    def testCellChanges(self):
        self.board.board[(1,1)] = 0
        self.assertEqual(self.board.cell_rule(1, 1), 1)

    def testCellIsStatic(self):
        self.board.board[(1,1)] = 3
        self.assertEqual(self.board.cell_rule(1, 1), 3)

    def testCellAtOrigin(self):
        # Test doesn't crash
        self.assertEqual(self.board.cell_rule(0, 0), 1)

        # Verify that it will detect a change of state
        self.board.board[(0,0)] = 1
        self.board.board[(1,0)] = 2
        self.assertEqual(self.board.cell_rule(0, 0), 2)

    def testCellAtFarCorner(self):
        # Test doesn't crash
        self.assertEqual(self.board.cell_rule(2, 2), 1)

        # Verify that it will detect a change of state
        self.board.board[(2,2)] = 1
        self.board.board[(2,1)] = 2
        self.assertEqual(self.board.cell_rule(2, 2), 2)


if __name__ == '__main__':
    unittest.main()

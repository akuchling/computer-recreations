#!/usr/bin/env python

import random
import operator

class Flib:
    def __init__(self):
        self.state = 'A'
        self.transitions = {}

    def predict(self, bit):
        "Given a bit, produce a predicted output and switch to new state"
        new_state, output = self.transitions[(self.state, bit)]
        self.state = new_state
        return output

    def reset(self):
        "Restore the flib to its starting state"
        self.state = 'A'

    def score(self, environment):
        """Given a flib and an environment string, test the flib on
        two repetitions of the environment and return the percentage
        of successes.
        """
        self.reset()
        matches = 0
        environment = [int(c) for c in environment]
        input = environment + environment
        expected = input[1:] + [input[0]]

        for (input_bit, expected_bit) in zip(input, expected):
            prediction = self.predict(input_bit)
            if prediction == expected_bit:
                matches += 1

        self.pct = matches / len(expected) * 100
        return self.pct

    def as_genome(self):
        "Return a string representation of the flib's genome"
        S = ''
        for key in sorted(self.transitions):
            new_state, output = self.transitions[key]
            S += new_state + str(output)
        return S

    def print_table(self):
        "Output the table version of the state machine"
        print('\t 0\t 1')
        for key in sorted(self.transitions):
            state, input = key
            new_state, output = self.transitions[key]
            if input == 0:
                print(state, '\t', new_state, ',', output, sep='', end='\t')
            else:
                print(new_state, ',', output, sep='')

    @classmethod
    def from_genome(cls, S):
        "Given the genome, create a corresponding Flib object"
        state = 'A'
        obj = cls()
        while S:
            new_state = S[0]
            output = int(S[1])
            obj.transitions[(state, 0)] = (new_state, output)

            new_state = S[2]
            output = int(S[3])
            obj.transitions[(state, 1)] = (new_state, output)

            state = chr(ord(state) + 1)
            S = S[4:]

        return obj

    @classmethod
    def random(cls, num_states):
        states = [chr(ord('A') + i) for i in range(num_states)]
        obj = cls()

        for state in states:
            obj.transitions[(state, 0)] = (random.choice(states),
                                           random.choice([0, 1]))
            obj.transitions[(state, 1)] = (random.choice(states),
                                           random.choice([0, 1]))

        return obj


def cross(flib1, flib2):
    gen1 = flib1.as_genome()
    gen2 = flib2.as_genome()
    cross1 = random.randrange(0, len(gen1))
    cross2 = random.randrange(0, len(gen1))
    # Swap so that cross1 is smaller
    if cross1 > cross2:
        cross1, cross2 = cross2, cross1

    new_genome = gen1[:cross1] + gen2[cross1:cross2] + gen1[cross2:]
    return Flib.from_genome(new_genome)

def mutate(genome):
    genome = list(genome)
    point = random.randrange(0, len(genome))

    if (point % 2) == 1:
        # Odd number, so the value switches between 0 and 1
        value = genome[point]
        genome[point] = str(1 - int(value))
    else:
        num_states = len(genome) / 4
        genome[point] = chr(ord('A') + random.randrange(0, num_states))

    return ''.join(genome)


def main():
    random.seed(1972)
    environment = '011001'

    # Generate 10 random flibs
    pool = [Flib.random(len(environment) // 2) for i in range(10)]

    # Score all the flibs
    for flib in pool:
        flib.score(environment)

    generation = 0
    while True:
        for flib in pool:
            flib.score(environment)
        print('Generation', generation)
        generation += 1
        # Sort flibs by score
        pool.sort(key=operator.attrgetter('pct'))
        best = pool[-1]
        print(' Best score in pool:', best.pct)
        worst = pool[0]
        print('Worst score in pool:', worst.pct)

        if best.pct == 100:
            print('Perfect score found!')
            break

        # Cross the best and the worst to make a new flib
        new_flib = cross(best, worst)

        # Replace the worst flib
        pool[0] = new_flib
        new_flib.score(environment)

        # Pick a flib to mutate
        flib = random.choice(pool)
        genome = flib.as_genome()
        new_genome = mutate(genome)
        new_flib = Flib.from_genome(new_genome)
        new_flib.score(environment)
        pool[1] = new_flib

    print('Environment being predicted=', environment)
    print(best.as_genome())
    best.print_table()


if __name__ == '__main__':
    main()

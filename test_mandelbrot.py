
import unittest
import mandelbrot

class TestMandelbrot(unittest.TestCase):
    def testExitsAtFirstIteration(self):
        self.assertEqual(mandelbrot.iterate(3), 1)

    def testNeverExits(self):
        self.assertEqual(mandelbrot.iterate(0), 0)

    def testCuspValue(self):
        "Just exercising a particular value that diverges slowly"
        self.assertEqual(mandelbrot.iterate(0.25 + .01), 30)

if __name__ == '__main__':
    unittest.main()

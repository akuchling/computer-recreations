
import numpy as np
from PIL import Image

def iterate(c, max_iterations=100):
    """Iterates z**2 + c, for up to max_iterations.  Returns 0 if the
    result doesn't diverage to infinity within that number of iterations,
    and returns a non-zero number otherwise.
    """
    z = 0j
    for i in range(max_iterations):
        z = z**2 + c
        if abs(z) > 2.0:
            return (i+1)
    else:
        return 0


class MandelbrotFrame:
    """Class representing a single displayed image.

    center: the coordinate of the center of the image (complex)
    width, height: float
     the image will go from center.real - width to center.real + width.

    """

    def __init__(self):
        self.center = -1 + 0j
        self.width = 1.5
        self.height = 1.5

    def get_center(self):
        return (self.center.real, self.center.imag)

    def set_to_aspect_ratio(self, ratio, width):
        """Set the width and height to a specified aspect ratio.
        The  aspect ratio is width/height.
        """
        self.height = width / ratio
        self.width = width

# Taken from http://www.color-hex.com/color-palette/14011
PALETTE = [
    (63,159,49),
    (137,219,171),
    (92,197,147),
    (100,191,106),
    (160,225,164),
    (255,255,255),
    ]


def determine_color(count, max_count):
    band = (max_count + 1) / len(PALETTE)
    if count == 0:
        return (0,0,0)
    else:
        choice = int(count // band)
        return PALETTE[choice]


def generate_image(im_width, im_height, frame):
    cx, cy = frame.get_center()
    x_inc, y_inc = (frame.width / (im_width / 2),
                    frame.height / (im_height / 2))

    grid = np.zeros((im_width, im_height))
    max_count = 0
    for i in range(im_width):
        for j in range(im_height):
            coord = complex((cx - frame.width) + i * x_inc,
                            (cy - frame.height) + j * y_inc)
            count = iterate(coord)
            max_count = max(max_count, count)
            grid[i,j] = count

    # Now that we've determined the counts, create an image
    image = Image.new('RGB', (im_width, im_height))
    for i in range(im_width):
        for j in range(im_height):
            color = determine_color(grid[i,j], max_count)
            image.putpixel((i,j), color)

    return image


def main():
    frame = MandelbrotFrame()
    frame.set_to_aspect_ratio(640/480, 2.0)
    im = generate_image(640, 480, frame)
    im.save('image.png')


if __name__ == '__main__':
    main()

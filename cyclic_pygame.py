
import sys
import random
import pygame

WIDTH = 260
HEIGHT = 260
CELL_SIZE = 3
##PALETTE = ['red', 'orange', 'yellow', 'green', 'blue', 'violet']
PALETTE = ['#1be7ff', '#6eeb83', '#e4ff1a', '#e8aa14', '#ff5714',
           '#5bc0eb', '#fde74c', '#9bc53d', '#e55934', '#fa7921',
           '#ff0000', '#ffdb00', '#cdfbbb', '#38edf2', '#b32eff',
           ]
NUM_STATES = len(PALETTE)

class Board:
    def __init__(self, width=WIDTH, height=HEIGHT):
        self.board = {}
        self.width = width
        self.height = height

    @classmethod
    def create_random_array(cls, width=WIDTH, height=HEIGHT):
        board = cls(width, height)
        for i in range(width):
            for j in range(height):
                board.board[(i,j)] = random.randrange(0, NUM_STATES)

        return board

    def print_board(self):
        for i in range(self.width):
            for j in range(self.height):
                print(self.board[(i,j)], sep='', end='')
            print()
        print('=======')

    def draw_graphics(self, scr):
        # Check if we need to exit.
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

        for i in range(self.width):
            for j in range(self.height):
                color = pygame.Color(PALETTE[ self.board[i,j] ])
                rect = pygame.Rect(i * CELL_SIZE, j * CELL_SIZE,
                                   i * CELL_SIZE + CELL_SIZE - 1,
                                   j * CELL_SIZE + CELL_SIZE - 1)
                pygame.draw.rect(scr, color, rect, 0)

        pygame.display.flip()

    def cell_rule(self, x, y):
        """Evaluate the rule for a single cell, returning the cell's new state
        (which can be the same as the old state): if the cell is in state N
        and a neighbour is in state N+1, the cell will enter state N+1 as well.
        """
        value = self.board[x,y]
        successor = (value + 1) % NUM_STATES
        indices = []
        for i in range(x-1, x+2):
            for j in range(y-1, y+2):
                if ((i,j) != (x,y) and
                    i >= 0 and j >= 0 and
                    i < self.width and j < self.height):
                    if self.board[(i, j)] == successor:
                        return successor
        else:
            return value


    def step(self):
        "Perform one iteration step of the cellular automaton"
        new = self.board.copy()
        for i in range(self.width):
            for j in range(self.height):
                new[(i,j)] = self.cell_rule(i, j)

        self.board.update(new)


if __name__ == '__main__':
    board = Board.create_random_array()
    pygame.init()
    screen = pygame.display.set_mode((board.width * CELL_SIZE, board.height * CELL_SIZE))
    while True:
        board.draw_graphics(screen)
        ##board.print_board()
        board.step()
        ##break

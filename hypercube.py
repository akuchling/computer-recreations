
import itertools
import math
import random
import sys

import numpy as np
import pygame

# Some constants
SIZE = 10.0                         # Size of cube

# Euler traversal of the vertices
TRAVERSAL_ORDER = [0, 1, 3, 2, 6, 14, 10, 8, 9, 11, 3, 7, 15, 14,
                   12, 13, 9, 1, 5, 7, 6, 4, 12, 8, 0, 4, 5, 13,
                   15, 11, 10, 2, 0]

# Make a list of the hypercube's vertices.
VERTICES = []

bitmasks = np.array([8, 4, 2, 1])
for i in range(16):
    # Turn 'i' into an array of bit values
    # e.g. i=10 -> (1, 0, 1, 0)
    bits = np.sign(np.full(4, i, dtype=int) & bitmasks)

    # Convert 0 to -SIZE and 1 to SIZE
    coord = bits * SIZE * 2 - SIZE
    VERTICES.append(np.matrix(coord).transpose())

def rotation_matrix(dim1, dim2, angle):
    """Return a rotation matrix
    """
    assert dim1 != dim2
    if dim1 > dim2:
        dim1, dim2 = dim2, dim1
    dim1 -= 1
    dim2 -= 1
    a = np.eye(4)
    a[dim1,dim1] = a[dim2,dim2] = math.cos(angle)
    a[dim1,dim2] = math.sin(angle)
    a[dim2,dim1] = -a[dim1,dim2]
    return np.matrix(a)

def calculate_points(rot):
    # Calculate rotation of hypercube
    rotated_vertices = [rot * vertex for vertex in VERTICES]

    # Do perspective projection to 2D
    projected_vertices = [(float(x),float(y)) for (x,y,z,t) in rotated_vertices]

    return projected_vertices


def main():
    pygame.init()
    scr_size = 640
    screen = pygame.display.set_mode((scr_size, scr_size))
    scale = (scr_size / 2) * .65 / (2*SIZE)

    angle = math.radians(1)
    black = pygame.Color('black')
    white = pygame.Color('#ffffff')

    cum_rotation = np.matrix(np.eye(4))
    while True:
        # Check if we need to exit.
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

        (dim1, dim2) = random.choice([(1,3), (1,2), (2,3), (3,4), (2,4)])
        rot = rotation_matrix(dim1, dim2, angle)
        cum_rotation = cum_rotation @ rot
        vertices = calculate_points(cum_rotation)

        screen.fill(black)
        cur_x, cur_y = vertices[TRAVERSAL_ORDER[0]]
        for index in TRAVERSAL_ORDER[1:]:
            x, y = vertices[index]
            pygame.draw.aaline(screen, white,
                              (cur_x * scale + scr_size/2, cur_y * scale + scr_size/2),
                              (x * scale + scr_size/2, y * scale + scr_size/2))
            cur_x, cur_y = x, y

        pygame.display.flip()


if __name__ == '__main__':
    main()


import random
import turtle

WIDTH = 100
HEIGHT = 100
PALETTE = ['red', 'orange', 'yellow', 'green', 'blue', 'violet']
NUM_STATES = len(PALETTE)

class Board:
    def __init__(self, width=WIDTH, height=HEIGHT):
        self.board = {}
        self.width = width
        self.height = height

    @classmethod
    def create_random_array(cls, width=WIDTH, height=HEIGHT):
        board = cls(width, height)
        for i in range(width):
            for j in range(height):
                board.board[(i,j)] = random.randrange(0, NUM_STATES)

        return board

    def print_board(self):
        for i in range(self.width):
            for j in range(self.height):
                print(self.board[(i,j)], sep='', end='')
            print()
        print('=======')

    def draw_graphics(self, scr):
        scr_width, scr_height = scr.window_width(), scr.window_height()
        t = turtle.Turtle()
        t.penup()
        t.hideturtle()
        x_grid = scr_width // self.width
        y_grid = scr_height // self.height
        for i in range(self.width):
            for j in range(self.height):
                color = PALETTE[ self.board[i,j] ]
                t.goto(i * x_grid + x_grid / 2 - scr_width / 2,
                       j * y_grid + y_grid / 2 - scr_height / 2)
                t.dot(min(x_grid, y_grid), color)

    def cell_rule(self, x, y):
        """Evaluate the rule for a single cell, returning the cell's new state
        (which can be the same as the old state): if the cell is in state N
        and a neighbour is in state N+1, the cell will enter state N+1 as well.
        """
        value = self.board[x,y]
        successor = (value + 1) % NUM_STATES
        indices = []
        for i in range(x-1, x+2):
            for j in range(y-1, y+2):
                if ((i,j) != (x,y) and
                    i >= 0 and j >= 0 and
                    i < self.width and j < self.height):
                    if self.board[(i, j)] == successor:
                        return successor
        else:
            return value


    def step(self):
        "Perform one iteration step of the cellular automaton"
        new = self.board.copy()
        for i in range(self.width):
            for j in range(self.height):
                new[(i,j)] = self.cell_rule(i, j)

        self.board.update(new)


if __name__ == '__main__':
    scr = turtle.Screen()
    board = Board.create_random_array()
    while True:
        board.draw_graphics(scr)
        ##board.print_board()
        board.step()
        ##break

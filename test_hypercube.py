
import unittest
import math
import numpy as np
import hypercube

class TestHypercube(unittest.TestCase):

    def testRotationMatrix(self):
        mat = hypercube.rotation_matrix(1, 3, math.pi / 2.0)
        arr = np.array(mat).round(3)
        self.assertEqual(list(arr[0]), [0.0, 0.0, 1.0, 0.0])
        self.assertEqual(list(arr[1]), [0.0, 1.0, 0.0, 0.0])
        self.assertEqual(list(arr[2]), [-1.0, 0.0, 0.0, 0.0])
        self.assertEqual(list(arr[3]), [0.0, 0.0, 0.0, 1.0])

if __name__ == '__main__':
    unittest.main()
